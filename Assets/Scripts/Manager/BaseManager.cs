using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseManager : MonoBehaviour
{
    protected GameplayManager gameplayManager;

    protected virtual void Awake()
    {
        gameplayManager = GameplayManager.Instance;
    }

    protected virtual void OnEnable()
    {
        GameplayManager.Instance.AddManager(this);
    }

    protected virtual void OnDisable()
    {
        if (GameplayManager.Instance)
        {
            GameplayManager.Instance.RemoveManager(this);
        }
    }
}
