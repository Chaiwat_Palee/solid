using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackHitBox : MonoBehaviour
{
    [SerializeField] private int damage = 0;
    [SerializeField] private SpriteRenderer sprite;
    
    public GameObject Host { get; set; }
    public AttackController attackController;

    private void OnTriggerEnter2D(Collider2D _col)
    {
        if (_col.gameObject == Host) return;

        damage = attackController.currentWeapon.Damage;
        
        if (_col.gameObject.TryGetComponent<IDamageable>(out var _damageable))
        {
            if (!_col.gameObject.TryGetComponent<Player>(out var _player))
            {
                _damageable.ApplyDamage(Host, damage);
            }
        }
    }
}
